import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import Bidang from "./BidangModel.js";
import Pegawai from "./PegawaiModel.js";

const { DataTypes } = Sequelize;

const Files = db.define('files', {
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
        validate: {
            notEmpty: true
        }
    },
    nama_files: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    files: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    jenis_file: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            notEmpty: true
        }
    },
    ext_file: {
        type: DataTypes.STRING(10),
        allowNull: false,
        validate: {
            notEmpty: true
        }
    }
}, {
    freezeTableName: true
});

Bidang.hasMany(Files);
Files.belongsTo(Bidang);
Pegawai.hasMany(Files);
Files.belongsTo(Pegawai);

export default Files;