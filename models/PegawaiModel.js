import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import Golongan from "./GolonganModel.js";
import Pangkat from "./PangkatModel.js";
import Bidang from "./BidangModel.js";

const { DataTypes } = Sequelize;

const Pegawai = db.define('pegawai', {
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
        validate: {
            notEmpty: true
        }
    },
    jenis_pegawai: {
        type: DataTypes.STRING(10),
        allowNull: true,
        validate: {
            notEmpty: false
        }
    },
    nip: {
        type: DataTypes.STRING(18),
        allowNull: true
    },
    name: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    tempat_lahir: {
        type: DataTypes.STRING(200),
        allowNull: false
    },
    tanggal_lahir: {
        type: DataTypes.STRING(11),
        allowNull: false
    },
    jabatan: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    masa_kerja_golongan: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    diklat: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    pendidikan: {
        type: DataTypes.STRING(200),
        allowNull: false
    },
    no_sk: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
            notEmpty: false
        }
    },
    no_rekening: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
            notEmpty: false
        }
    },
    nama_rekening: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
            notEmpty: false
        }
    },
    umur: {
        type: DataTypes.INTEGER(2),
        allowNull: false
    },
    jenis_kelamin: {
        type: DataTypes.STRING(10),
        allowNull: false
    },
    agama: {
        type: DataTypes.STRING(10),
        allowNull: false
    },
    kenaikan_pangkat: {
        type: DataTypes.STRING(100),
        allowNull: true
    },
    batas_pensiun: {
        type: DataTypes.INTEGER(4),
        allowNull: true
    },
    foto: {
        type: DataTypes.STRING
    },
    url: {
        type: DataTypes.STRING
    }
}, {
    freezeTableName: true
});

Pangkat.hasOne(Pegawai);
Pegawai.belongsTo(Pangkat);
Golongan.hasOne(Pegawai);
Pegawai.belongsTo(Golongan);
Bidang.hasMany(Pegawai)
Pegawai.belongsTo(Bidang);

export default Pegawai;