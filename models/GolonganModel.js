import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const {DataTypes} = Sequelize;

const Golongan = db.define('golongan', {
    uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
        validate: {
            notEmpty: true
        }
    },
    nama_golongan: {
        type: DataTypes.STRING(150),
        allowNull: false
    }
},{
    freezeTableName: true,
});

export default Golongan;