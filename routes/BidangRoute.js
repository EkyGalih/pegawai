import express from "express";
import {
    getAllBidang,
    getBidang,
    getBidangById,
    saveBidang,
    updateBidang,
    deleteBidang
} from "../controllers/BidangController.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/bidangs', verifyUser, getAllBidang);
router.get('/bidang', verifyUser, getBidang);
router.get('/bidang/:id', verifyUser, getBidangById);
router.post('/bidang', verifyUser, saveBidang);
router.patch('/bidang/:id', verifyUser, updateBidang);
router.delete('/bidang/:id', verifyUser, deleteBidang);

export default router;