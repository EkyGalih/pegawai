import express from "express";
import {
    getAllPangkat,
    getPangkat,
    getPangkatById,
    savePangkat,
    updatePangkat,
    deletPangkat
} from "../controllers/PangkatController.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/pangkats', verifyUser, getAllPangkat);
router.get('/pangkat', verifyUser, getPangkat);
router.get('/pangkat/:id', verifyUser, getPangkatById);
router.post('/pangkat', verifyUser, savePangkat);
router.patch('/pangkat/:id', verifyUser, updatePangkat);
router.delete('/pangkat/:id', verifyUser, deletPangkat);

export default router;