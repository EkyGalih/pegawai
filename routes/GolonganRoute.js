import express from "express";
import {
    getAllGolongan,
    getGolongan,
    getGolonganById,
    saveGolongan,
    updateGolongan,
    deletGolongan
} from "../controllers/GolonganController.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/golongans', verifyUser, getAllGolongan);
router.get('/golongan', verifyUser, getGolongan);
router.get('/golongan/:id', verifyUser, getGolonganById);
router.post('/golongan', verifyUser, saveGolongan);
router.patch('/golongan/:id', verifyUser, updateGolongan);
router.delete('/golongan/:id', verifyUser, deletGolongan);

export default router;