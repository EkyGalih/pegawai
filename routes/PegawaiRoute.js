import express from "express";
import {
    getAllPegawai,
    getPegawaiById,
    savePegawai,
    updatePegawai,
    deletePegawai,
    getPegawaiAsn,
    getPegawaiNonAsn
} from "../controllers/PegawaiController.js";
import { verifyUser } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/pegawai', verifyUser, getAllPegawai);
router.get('/pegawaiAsn', getPegawaiAsn);
router.get('/pegawaiNonAsn', getPegawaiNonAsn);
router.get('/pegawai/:id', getPegawaiById);
router.post('/pegawai', verifyUser, savePegawai);
router.patch('/pegawai/:id', verifyUser, updatePegawai);
router.delete('/pegawai/:id', verifyUser, deletePegawai);

export default router;