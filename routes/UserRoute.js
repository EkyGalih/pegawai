import express from "express";
import {
    getUsers,
    getUserById,
    saveUser,
    updateUser,
    deletUser
} from "../controllers/UserController.js";
import { verifyUser, adminOnly } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/users', adminOnly, verifyUser, getUsers);
router.get('/users/:id', adminOnly, verifyUser, getUserById);
router.post('/users', adminOnly, verifyUser, saveUser);
router.patch('/users/:id', adminOnly, verifyUser, updateUser);
router.delete('/users/:id', adminOnly, verifyUser, deletUser);

export default router;