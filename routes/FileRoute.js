import express from "express";
import {
    getAllFiles,
    getFiles,
    getFileById,
    createFile,
    updateFile,
    deleteFile
} from "../controllers/FileController.js";
import { verifyUser, adminOnly } from "../middleware/AuthUser.js";

const router = express.Router();

router.get('/files', adminOnly, verifyUser, getAllFiles);
router.get('/file', getFiles);
router.get('/file/:id', adminOnly, verifyUser, getFileById);
router.post('/file', adminOnly, verifyUser, createFile);
router.patch('/file/:id', adminOnly, verifyUser, updateFile);
router.delete('/file/:id', adminOnly, verifyUser, deleteFile);

export default router;