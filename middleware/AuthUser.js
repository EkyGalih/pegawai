import User from "../models/UserModel.js";

export const verifyUser = async (req, res, next) => {
    if (!req.session.userUuid) {
        return res.status(401).json({msg: "Please login to your account"});
    }
    const user = await User.findOne({
        where: {
            uuid: req.session.userUuid
        }
    });
    if (!user) return res.status(404).json({msg: "User not found!"});
    req.userUuid = user.uuid;
    req.role = user.role;
    next();
}

export const adminOnly = async (req, res, next) => {
    const user = await User.findOne({
        where: {
            uuid: req.session.userUuid
        }
    });
    if (!user) return res.status(404).json({msg: "User not found!"});
    if (user.role !== "admin") return res.status(403).json({msg: "Access Denied!"});
    next();
}