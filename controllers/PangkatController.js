import { Op } from "sequelize";
import Pangkat from "../models/PangkatModel.js";

export const getAllPangkat = async (req, res) => {
    try {
        const response = await Pangkat.findAll({
            attributes: ['uuid', 'nama_pangkat'],
            order: [
                ['createdAt', 'DESC']
        ]
        });
        res.json(response);
    } catch (error) {
        console.log(error);
    }
}


export const getPangkat = async(req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 5;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await Pangkat.count({
        where: {
            [Op.or]: [{nama_pangkat:{
                [Op.like]: '%'+search+'%'
            }}]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Pangkat.findAll({
        where: {
            [Op.or]: [{nama_pangkat:{
                [Op.like]: '%'+search+'%'
            }}]
        },
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getPangkatById = async(req, res) => {
    try {
        const response = await Pangkat.findOne({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json(response);
    } catch (error) {
        console.log(error.message);
    }
}

export const savePangkat = async(req, res) => {
    const {nama_pangkat} = req.body;
    try {
        await Pangkat.create({
            nama_pangkat: nama_pangkat
        });
        res.status(201).json({msg: "Pangkat Created"});
    } catch (error) {
        console.log(error.message);
    }
}

export const updatePangkat = async(req, res) => {
    const pangkat = await Pangkat.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!pangkat) return res.status(404).json({msg: "Pangkat not Found"});
    const {nama_pangkat} = req.body;
    try {
        await Pangkat.update({nama_pangkat: nama_pangkat},{
            where: {
                uuid: pangkat.uuid
            }
        });
        res.status(200).json({msg: "Pangkat Updated"});
    } catch (error) {
        console.log(error.message);
    }
}

export const deletPangkat = async(req, res) => {
    const pangkat = await Pangkat.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!pangkat) return res.status(404).json({msg: "Pangkat not Found"});
    try {
        await Pangkat.destroy({
            where: {
                uuid: pangkat.uuid
            }
        });
        res.status(200).json({msg: "Pangkat Deleted"});
    } catch (error) {
        console.log(error.message);
    }
}