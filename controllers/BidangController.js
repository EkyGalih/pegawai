import { Op } from "sequelize";
import Bidang from "../models/BidangModel.js";

export const getAllBidang = async (req, res) => {
    try {
        const response = await Bidang.findAll({
            attributes: ['uuid', 'nama_bidang']
        });
        res.json(response);
    } catch (error) {
        console.log(error);
    }
}

export const getBidang = async(req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 5;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await Bidang.count({
        where: {
            [Op.or]: [{nama_bidang:{
                [Op.like]: '%'+search+'%'
            }}]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Bidang.findAll({
        where: {
            [Op.or]: [{nama_bidang:{
                [Op.like]: '%'+search+'%'
            }}]
        },
        offset: offset,
        limit: limit,
        order:[
            ['nama_bidang', 'ASC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getBidangById = async(req, res) => {
    try {
        const response = await Bidang.findOne({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json(response);
    } catch (error) {
        console.log(error);
    }
}

export const saveBidang = async(req, res) => {
    const {nama_bidang} = req.body;
    try {
        await Bidang.create({
            nama_bidang: nama_bidang
        });
        res.status(201).json({msg: "Bidang Created!"});
    } catch (error) {
        console.log(error);
    }
}

export const updateBidang = async(req, res) => {
    console.log(req.params);
    const bidang = await Bidang.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!bidang) return res.status(404).json({msg: "Bidang not found!"});
    const {nama_bidang} = req.body;
    try {
        await Bidang.update({nama_bidang: nama_bidang},{
            where: {
                uuid: bidang.uuid
            }
        });
        res.status(200).json({msg: "Bidang Updated!"});
    } catch (error) {
        console.log(error);
    }
}

export const deleteBidang = async(req, res) => {
    const bidang = await Bidang.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!bidang) return res.status(404).json({msg: "Bidang not found!"});
    try {
        await Bidang.destroy({
            where: {
                uuid: bidang.uuid
            }
        });
        res.status(200).json({msg: "Bidang Deleted!"});
    } catch (error) {
        console.log(error);
    }
}