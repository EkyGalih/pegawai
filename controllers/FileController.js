import { Op } from "sequelize";
import path from "path";
import fs from "fs";
import File from "../models/FilesModel.js";
import Bidang from "../models/BidangModel.js";
import Pegawai from "../models/PegawaiModel.js";

export const getAllFiles = async (req, res) => {
    try {
        const response = await File.findAll({
            order: [
                ['createdAt', 'DESC']
            ]
        });
        res.json(response);
    } catch (error) {
        res.status(404).json({msg: error.message});
    }
}

export const getFiles = async (req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 10;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await File.count({
        where: {
            [Op.or]: [{
                nama_files: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await File.findAll({
        include: [{
            model: Bidang,
            as: 'bidang',
            attributes: ['nama_bidang']
        }, {
            model: Pegawai,
            as: 'pegawai',
            attributes: ['name']
        }],
        where: {
            [Op.or]: [{
                nama_files: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        },
        offset: offset,
        limit: limit,
        order: [
            ['nama_files', 'ASC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getFileById = async (req, res) => {
    try {
        const response = await File.findOne({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json(response);
    } catch (error) {
        res.status(404).json({msg: error.message});
    }
}

export const createFile = async (req, res) => {
    if (req.files === null) return res.status(404).json({ msg: "No File Uploaded" });
    const file = req.files.files;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.name;
    const files = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/files/${files}`;
    const allowedType = ['.pdf', '.xls', '.xlsx', '.doc', '.docx', '.csv'];

    const { jenis_file, ext_file, bidangUuid, pegawaiUuid } = req.body;

    if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Files" });
    if (fileSize > 5000000) return res.status(422).json({ msg: "Files must be less than 5MB" });

    file.mv(`./public/files/${files}`, async (err) => {
        if (err) return res.status(500).json({ msg: err.message });
        try {
            await File.create({
                nama_files: fileName,
                files: files,
                url: url,
                jenis_file: jenis_file,
                ext_file: ext_file,
                bidangUuid: bidangUuid,
                pegawaiUuid: pegawaiUuid
            });
            res.status(201).json({ msg: "Files Uploaded!" });
        } catch (error) {
            res.status(400).json({msg: error.message});
        }
    })
}

export const updateFile = async (req, res) => {
    const filee = await File.findOne({
        where: {
            uuid: req.params.id
        }
    });

    if (!filee) return res.status(404).json({ msg: "No Data Found" });
    let files = "";
    let fileName = "";
    if (req.files === null) {
        files = filee.files;
        fileName = filee.nama_files;
    } else {
        const file = req.files.files;
        const fileSize = file.data.length;
        const ext = path.extname(file.name);
        files = file.md5 + ext;
        fileName = file.name;
        const allowedType = ['.pdf', '.xls', '.xlsx', '.doc', '.docx', '.csv'];

        if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Files" });
        if (fileSize > 5000000) return res.status(422).json({ msg: "Files must be less than 5MB" });
        console.log('ini file ' + filee);
        const filepath = `./public/files/${filee.files}`;
        fs.unlinkSync(filepath);

        file.mv(`./public/files/${files}`, (err) => {
            if (err) return res.status(500).json({ msg: err.message });
        });
    }
    const url = `${req.protocol}://${req.get("host")}/files/${files}`;

    const { jenis_file, ext_file, bidangUuid, pegawaiUuid } = req.body;

    try {
        await File.update({
            nama_files: fileName,
            files: files,
            url: url,
            jenis_file: jenis_file,
            ext_file: ext_file,
            bidangUuid: bidangUuid,
            pegawaiUuid: pegawaiUuid
        }, {
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json({ msg: "Files updated successfully" });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
}

export const deleteFile = async (req, res) => {
    const file = await File.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!file) return res.status(404).json({ msg: "No Data Found" });

    try {
        const filepath = `./public/files/${file.files}`;
        fs.unlinkSync(filepath);
        await File.destroy({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json({ msg: "File Deleted Successfully" });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
}