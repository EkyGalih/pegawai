import { Op } from "sequelize";
import Pegawai from "../models/PegawaiModel.js";
import User from "../models/UserModel.js";
import argon2 from "argon2";

export const getUsers = async (req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 5;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await User.count({
        where: {
            [Op.or]: [{
                username: {
                    [Op.like]: '%' + search + '%'
                }
            }, {
                email: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await User.findAll({
        include: [{
            model: Pegawai,
            as: 'pegawai',
            attributes: ['name', 'nip', 'foto', 'url']
        }],
        where: {
            [Op.or]: [{
                username: {
                    [Op.like]: '%' + search + '%'
                }
            }, {
                email: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        },
        offset: offset,
        limit: limit,
        order: [
            ['createdAt', 'DESC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}
export const getUserById = async (req, res) => {
    try {
        const response = await User.findOne({
            include: [{
                model: Pegawai,
                attributes: ['uuid', 'name', 'nip', 'foto', 'url']
            }],
            attributes: ['uuid', 'username', 'email', 'role', 'pegawaiUuid'],
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json(response);
    } catch (error) {
        console.log(error.message);
    }
}

export const saveUser = async (req, res) => {
    const { username, email, password, confPassword, role, pegawaiUuid } = req.body;
    if (password !== confPassword) return res.status(400).json({ msg: "Password and Confirm Password didn't match" });
    const hashPassword = await argon2.hash(password);
    try {
        await User.create({
            username: username,
            email: email,
            password: hashPassword,
            role: role,
            pegawaiUuid: pegawaiUuid
        });
        res.status(201).json({ msg: "User created successfully" });
    } catch (error) {
        res.status(400).json({ msg: error.message });
    }
}

export const updateUser = async (req, res) => {
    const user = await User.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!user) return res.status(404).json({ msg: "User not found!" });
    const { username, email, password, confPassword, role, pegawaiUuid } = req.body;
    console.log(email);
    let hashPassword;
    if (password === "" || password === null) {
        hashPassword = user.password;
    } else {
        hashPassword = await argon2.hash(password);
    }
    if (password !== confPassword) return res.status(400).json({ msg: "Password and Confirm Password didn't match" });
    try {
        await User.update({
            username: username,
            email: email,
            password: hashPassword,
            role: role,
            pegawaiUuid: pegawaiUuid
        }, {
            where: {
                uuid: user.uuid
            }
        });
        res.status(200).json({ msg: "User update successfully" });
    } catch (error) {
        res.status(400).json({ msg: error.message });
    }
}

export const deletUser = async (req, res) => {
    const user = await User.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!user) return res.status(404).json({ msg: "User not found!" });
    try {
        await User.destroy({
            where: {
                uuid: user.uuid
            }
        });
        res.status(200).json({ msg: "User delete successfully" });
    } catch (error) {
        res.status(400).json({ msg: error.message });
    }
}