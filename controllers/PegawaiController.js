import Pegawai from "../models/PegawaiModel.js";
import path from "path";
import { Op } from "sequelize";
import Golongan from "../models/GolonganModel.js";
import Pangkat from "../models/PangkatModel.js";
import Bidang from "../models/BidangModel.js";

export const getAllPegawai = async (req, res) => {
    try {
        const response = await Pegawai.findAll({
            attributes: ['uuid', 'name']
        });
        res.json(response);
    } catch (error) {
        console.log(error);
    }
}

export const getPegawaiAsn = async (req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 8;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await Pegawai.count({
        where: {
            [Op.or]: [{
                nip: {
                    [Op.like]: '%' + search + '%'
                }
            }, {
                name: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Pegawai.findAll({
        include: [{
            model: Golongan,
            as: 'golongan',
            attributes: ['nama_golongan']
        }, {
            model: Pangkat,
            as: 'pangkat',
            attributes: ['nama_pangkat']
        }],
        where: {
            jenis_pegawai: 'pns',
            [Op.or]: [{
                nip: {
                    [Op.like]: '%' + search + '%'
                }
            }, {
                name: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        },
        offset: offset,
        limit: limit,
        order: [
            ['name', 'ASC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getPegawaiNonAsn = async (req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 8;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await Pegawai.count({
        where: {
            [Op.or]: [{
                name: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Pegawai.findAll({
        include: [{
            model: Golongan,
            as: 'golongan',
            attributes: ['nama_golongan']
        }, {
            model: Pangkat,
            as: 'pangkat',
            attributes: ['nama_pangkat']
        }],
        where: {
            jenis_pegawai: 'kontrak',
            [Op.or]: [{
                name: {
                    [Op.like]: '%' + search + '%'
                }
            }]
        },
        offset: offset,
        limit: limit,
        order: [
            ['name', 'ASC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getPegawaiById = async (req, res) => {
    try {
        const response = await Pegawai.findOne({
            include: [{
                model: Golongan
            }, {
                model: Pangkat
            }, {
                model: Bidang
            }],
            where: {
                uuid: req.params.id
            }
        });
        res.json(response);
    } catch (error) {
        console.log(error.message);
    }
}

export const savePegawai = (req, res) => {
    if (req.files === null) return res.status(400).json({ msg: "No File Uplaoded" });
    const nip = req.body.nip;
    const name = req.body.name;
    const file = req.files.file;
    const fileSize = file.data.length;
    const ext = path.extname(file.name);
    const fileName = file.md5 + ext;
    const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;
    const allowedType = ['.png', '.jpg', '.jpeg'];

    const { jenis_pegawai, tempat_lahir, tanggal_lahir, jabatan, masa_kerja_golongan, diklat, pendidikan, no_sk, no_rekening, nama_rekening, umur, jenis_kelamin, agama, kenaikan_pangkat, batas_pensiun, pangkatUuid, golonganUuid, bidangUuid } = req.body;

    if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Image" });
    if (fileSize > 5000000) return res.status(422).json({ msg: "Image must be less than 5MB" });

    file.mv(`./public/images/${fileName}`, async (err) => {
        if (err) return res.status(500).json({ msg: err.message });
        try {
            await Pegawai.create({
                jenis_pegawai: jenis_pegawai,
                nip: nip,
                name: name,
                tempat_lahir: tempat_lahir,
                tanggal_lahir: tanggal_lahir,
                jabatan: jabatan,
                masa_kerja_golongan: masa_kerja_golongan,
                diklat: diklat,
                pendidikan: pendidikan,
                no_sk: no_sk,
                no_rekening: no_rekening,
                nama_rekening: nama_rekening,
                umur: umur,
                jenis_kelamin: jenis_kelamin,
                agama: agama,
                kenaikan_pangkat: kenaikan_pangkat,
                batas_pensiun: batas_pensiun,
                pangkatUuid: pangkatUuid,
                golonganUuid: golonganUuid,
                bidangUuid: bidangUuid,
                foto: fileName,
                url: url
            });
            res.status(201).json({ msg: "Pegawai Created Successfully" });
        } catch (error) {
            console.log(error.message);
        }
    })
}

export const updatePegawai = async (req, res) => {
    const pegawai = await Pegawai.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!pegawai) return res.status(404).json({ msg: "No Data Found" });
    let fileName = "";
    if (req.files === null) {
        fileName = pegawai.foto;
    } else {
        const file = req.files.file;
        const fileSize = file.data.length;
        const ext = path.extname(file.name);
        fileName = file.md5 + ext;
        const allowedType = ['.png', '.jpg', '.jpeg'];

        if (!allowedType.includes(ext.toLowerCase())) return res.status(422).json({ msg: "Invalid Image" });
        if (fileSize > 5000000) return res.status(422).json({ msg: "Image must be less than 5MB" });

        // const filepath = `./public/images/${pegawai.foto}`;
        // fs.unlinkSync(filepath);

        file.mv(`./public/images/${fileName}`, (err) => {
            if (err) return res.status(500).json({ msg: err.message });
        });
    }
    const nip = req.body.nip;
    const name = req.body.name;
    const url = `${req.protocol}://${req.get("host")}/images/${fileName}`;

    const { jenis_pegawai, tempat_lahir, tanggal_lahir, jabatan, masa_kerja_golongan, diklat, pendidikan, no_sk, nama_rekening, no_rekening, umur, jenis_kelamin, agama, kenaikan_pangkat, batas_pensiun, pangkatUuid, golonganUuid, bidangUuid } = req.body;

    try {
        await Pegawai.update({
            jenis_pegawai: jenis_pegawai,
            nip: nip,
            name: name,
            tempat_lahir: tempat_lahir,
            tanggal_lahir: tanggal_lahir,
            jabatan: jabatan,
            masa_kerja_golongan: masa_kerja_golongan,
            diklat: diklat,
            pendidikan: pendidikan,
            no_sk: no_sk,
            nama_rekening: nama_rekening,
            no_rekening: no_rekening,
            umur: umur,
            jenis_kelamin: jenis_kelamin,
            agama: agama,
            kenaikan_pangkat: kenaikan_pangkat,
            batas_pensiun: batas_pensiun,
            pangkatUuid: pangkatUuid,
            golonganUuid: golonganUuid,
            bidangUuid: bidangUuid,
            foto: fileName,
            url: url
        }, {
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json({ msg: "Pegawai updated successfully" });
    } catch (error) {
        console.log(error.message);
    }

}

export const deletePegawai = async (req, res) => {
    const pegawai = await Pegawai.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!pegawai) return res.status(404).json({ msg: "No Data Found" });

    try {
        // const filepath = `./public/images/${pegawai.foto}`;
        // fs.unlinkSync(filepath);
        await Pegawai.destroy({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json({ msg: "Pegawai Deleted Successfully" });
    } catch (error) {
        console.log(error.message);
    }
}