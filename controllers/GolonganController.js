import Golongan from "../models/GolonganModel.js";
import { Op } from "sequelize";

export const getAllGolongan = async (req, res) => {
    try {
        const response = await Golongan.findAll({
            attributes: ['uuid', 'nama_golongan'],
            order: [
                ['createdAt', 'DESC']
            ]
        });
        res.json(response);
    } catch (error) {
        console.log(error);
    }
}


export const getGolongan = async(req, res) => {
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 5;
    const search = req.query.search_query || "";
    const offset = limit * page;
    const totalRows = await Golongan.count({
        where: {
            [Op.or]: [{nama_golongan:{
                [Op.like]: '%'+search+'%'
            }}]
        }
    });
    const totalPage = Math.ceil(totalRows / limit);
    const result = await Golongan.findAll({
        where: {
            [Op.or]: [{nama_golongan:{
                [Op.like]: '%'+search+'%'
            }}]
        },
        offset: offset,
        limit: limit,
        order:[
            ['createdAt', 'DESC']
        ]
    });
    res.json({
        result: result,
        page: page,
        limit: limit,
        totalRows: totalRows,
        totalPage: totalPage
    });
}

export const getGolonganById = async(req, res) => {
    try {
        const response = await Golongan.findOne({
            where: {
                uuid: req.params.id
            }
        });
        res.status(200).json(response);
    } catch (error) {
        console.log(error.message);
    }
}

export const saveGolongan = async(req, res) => {
    const {nama_golongan} = req.body;
    try {
        await Golongan.create({
            nama_golongan: nama_golongan
        });
        res.status(201).json({msg: "Golongan Created!"});
    } catch (error) {
        console.log(error.message);
    }
}

export const updateGolongan = async(req, res) => {
    const golongan = await Golongan.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!golongan) return res.status(404).json({msg: "Golongan not found!"});
    const {nama_golongan} = req.body;
    try {
        await Golongan.update({nama_golongan: nama_golongan},{
            where: {
                uuid: golongan.uuid
            }
        });
        res.status(200).json({msg: "Golongan Updated!"});
    } catch (error) {
        console.log(error.message);        
    }
}

export const deletGolongan = async(req, res) => {
    const golongan = await Golongan.findOne({
        where: {
            uuid: req.params.id
        }
    });
    if (!golongan) return res.status(404).json({msg: "Golongan not found!"});
    try {
        await Golongan.destroy({
            where: {
                uuid: golongan.uuid
            }
        });
        res.status(200).json({msg: "Golongan Deleted!"});
    } catch (error) {
        console.log(error.message);        
    }
}