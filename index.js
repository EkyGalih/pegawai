import express from "express";
import FileUpload from "express-fileupload";
import cors from "cors";
import session from "express-session";
import dotenv from "dotenv";
import db from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";
import PangkatRoute from "./routes/PangkatRoute.js";
import GolonganRoute from "./routes/GolonganRoute.js";
import PegawaiRoute from "./routes/PegawaiRoute.js";
import UserRoute from "./routes/UserRoute.js";
import BidangRoute from "./routes/BidangRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
import FileRoute from "./routes/FileRoute.js";
dotenv.config();

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
    db: db
});

// (async () => {
//     await db.sync();
// })();

app.use(cors({
    credentials: true,
    origin: 'http://localhost:3000'
}));

app.use(session({
    secret: process.env.SESS_SECRET,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
        secure: 'auto'
    }
}));

app.use(express.json());
app.use(FileUpload());
app.use(express.static("public"));
app.use(PegawaiRoute);
app.use(PangkatRoute);
app.use(GolonganRoute);
app.use(UserRoute);
app.use(BidangRoute);
app.use(FileRoute);
app.use(AuthRoute);

// store.sync();

app.listen(process.env.APP_PORT, () => {
    console.log('server up and running...');
});